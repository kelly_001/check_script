<?php

function getPage($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    if (!($data = curl_exec($curl)))
        //echo "page: ".$data;
        echo "error" . curl_error($curl);
    curl_close($curl);
    return $data;
}

$sitemap_url = "";
$site_url = "";
$check_url = "http://antivirus-alarm.ru/proverka/?url=";
$urls = array();

// читаем данные с формы
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['sitemap'])) {
        $sitemap_url = $_POST['sitemap'];
        $site_url = $_POST['url'];
        $sitemap = getPage($sitemap_url);
        $html = "";
        $pattern = '/((http|https)\:\/\/|www)[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\.\/\?\:@\-_=#])*/';
        $pattern1 = '/((http|https)\:\/\/(www.)?|www.)'.$site_url.'([a-zA-Z0-9\.\/\?\:@\-_=#]+(\.[a-zA-Z0-9\.\/\?\:@\-_=#]+)?)*\/?/';
        $match_count = preg_match_all($pattern1, $sitemap, $matches, PREG_SET_ORDER);
        IF ($match_count) {
            foreach($matches as $match) {
                array_push( $urls, $match[0]);
/*                echo ("<iframe width='80%'  align='center' name = '$match[0]_frame'></iframe>");
                echo ("<form hidden id='drwebscanformURL' action='http://online.us.drweb.com/result/' method='post' target='$match[0]_frame'>
                    <input type='text' name='url' value='$match[0]' size='80' id='url' />
                    <input type='submit'  value='Проверить' />
                </form>");*/
            }
        }
        //print_r($urls);
    }
} else {
    header("Location: http://1ps.ru/");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Проверка сайтов на вирусы. Результат </title>

    <!-- Bootstrap -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<h1>Проверяем все страницы сайта <strong> <?echo $site_url?> </strong></h1>

<?php foreach ($urls AS $url):?>
    <iframe width='80%'  align='center' name = '<?Echo($url)?>_frame'></iframe>
    <form hidden id='drwebscanformURL' action='http://online.us.drweb.com/result/' method='post'
          target='<?Echo($url)?>_frame'>
                    <input type='text' name='url' value='<?Echo($url)?>' size='80' id='url' />
                    <input type='submit'  value='Проверить' />
                </form>
<?endforeach?>

<p>
    Результат проверки:
    <span id="result"></span>
</p>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("form").each(function(form){
            $(this).submit().delay(2000);
        });
    });
</script>
</body>
</html>

